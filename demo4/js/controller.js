var artistControllers = angular.module('artistControllers', ['ngAnimate']); //namespace, ngAnimate added as a dependency
artistControllers.controller('ListController', ['$scope', '$http', function ($scope, $http){ //protect $scope and $http from minification
    $http.get('js/data.json').success(function(data){
        $scope.artists = data;
        $scope.artistOrder = 'name';
    });
}]);
artistControllers.controller('DetailsController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams){ //protect $scope and $http from minification
    $http.get('js/data.json').success(function(data){
        $scope.artists = data;
        $scope.whichItem = $routeParams.itemId;
        
        if($routeParams.itemId > 0){
            $scope.prevItem = Number($routeParams.itemId)-1;
        }else{
            $scope.prevItem = $scope.artists.length-1;
        }
        if($routeParams.itemId == $scope.artists.length-1){
            $scope.nextItem = 0;
        }else{
            $scope.nextItem = Number($routeParams.itemId)+1;
        }
    });
}]);