var demo4App = angular.module('demo4App', ['ngRoute', 'artistControllers']);

demo4App.config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/list', {    //service like $http
        templateUrl: 'partials/list.html',
        controller: 'ListController'
    }).when('/details/:itemId',{
      templateUrl:'partials/details.html',
      controller: 'DetailsController'
    }).otherwise({
        redirectTo: '/list'
    });    
}]); 