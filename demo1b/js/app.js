var demo1App = angular.module('demo1App', []); //namespace

demo1App.directive('enter', function(){ 
    return function(scope, element, attrs){
        element.bind("mouseenter", function(){
           element.addClass(attrs.enter) 
        });
    }
});
demo1App.directive('leave', function(){ 
    return function(scope, element, attrs){
        element.bind("mouseleave", function(){
           element.removeClass(attrs.enter) 
        });
    }
});
