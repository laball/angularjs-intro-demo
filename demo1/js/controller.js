var demo1App = angular.module('demo1App', []); //namespace
demo1App.controller('demo1Controller', function demo1Controller($scope){
    $scope.author = {
        'name' : 'Larry Ball',
        'title' : 'Web Developer',
        'company' : 'USDM'
    };
});