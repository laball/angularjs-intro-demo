var demo1App = angular.module('demo1App', []); //namespace

demo1App.factory('Data', function(){ //creates a service
    return{message: "I'm data from a service"};
});

demo1App.filter('reverse', function(Data){
    return function(text){
        return text.split("").reverse().join("");
    }
});

demo1App.controller('demo1FirstController', function ($scope, Data){
    $scope.data = Data;
});
demo1App.controller('demo1SecondController', function ($scope, Data){
    $scope.data = Data;
    $scope.reversedMessage = function(message){
        return message.split("").reverse().join("");
    }
});