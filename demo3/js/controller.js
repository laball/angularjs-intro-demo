var demo2App = angular.module('demo2App', []); //namespace
demo2App.controller('demo2Controller', ['$scope', '$http', function ($scope, $http){ //protect $scope and $http from minification
    $http.get('js/data.json').success(function(data){
        $scope.artists = data;
        $scope.artistOrder = 'name';
    });
}]);